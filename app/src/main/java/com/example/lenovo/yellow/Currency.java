package com.example.lenovo.yellow;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Currency implements Parcelable{
    private String base;
    private String date;
    private double[] rates;
    private int icon;
    private String shortInfo;


//to do
    public Currency(int icon, String base, String date, String shortInfo){ //double[] rates) {
        this.base = base;
        this.date = date;
        this.rates = rates;
        this.shortInfo = shortInfo;
    }

    public String getBase() {
        return base;
    }

    public String getDate() {
        return date;
    }

    public double[] getRates() {
        return rates;
    }

    public int getIcon() {
        return icon;
    }

    public String getShortInfo() {
        return shortInfo;
    }


    protected Currency(Parcel in) {
        base = in.readString();
        date = in.readString();
        icon = in.readInt();
    }

    public static final Creator<Currency> CREATOR = new Creator<Currency>() {
        @Override
        public Currency createFromParcel(Parcel in) {
            return new Currency(in);
        }

        @Override
        public Currency[] newArray(int size) {
            return new Currency[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(base);
        dest.writeString(date);
        dest.writeInt(icon);
    }
}
