package com.example.lenovo.yellow;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Main2Activity extends AppCompatActivity {

    Currency currency;

    @BindView(R.id.name_currency)
    TextView currencyName;

    @BindView(R.id.info_currency)
    TextView currencyInfo;

    @BindView(R.id.full_info_currency)
    TextView currencyFullInfo;

    @BindView(R.id.image_currency)
    ImageView currencyIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ButterKnife.bind(this);

        if(getIntent()!= null && getIntent().getParcelableExtra(MainActivity.KEY_VALUE) != null){
            currency = getIntent().getParcelableExtra(MainActivity.KEY_VALUE);

            currencyName.setText(currency.getBase());
            currencyInfo.setText(currency.getShortInfo());
            currencyFullInfo.setText(currency.getShortInfo());

            currencyIcon.setImageResource(currency.getIcon());

        }
    }
}
