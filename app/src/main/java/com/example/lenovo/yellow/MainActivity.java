package com.example.lenovo.yellow;

import android.content.Intent;
import android.support.annotation.IntRange;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {

    ListAdapter myAdapter;

    public static final String KEY_VALUE = "key";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Toast.makeText(MainActivity.this, "hello", Toast.LENGTH_SHORT).show();

        myAdapter = new ListAdapter(this, generateList());
        ListView listView = findViewById(R.id.list_currency);
        listView.setAdapter(myAdapter);
    }

    @OnItemClick(R.id.list_currency)
    public void onItemClick(int position) {
        Toast.makeText(MainActivity.this, myAdapter.getItem(position).getBase(), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, Main2Activity.class);
        intent.putExtra(KEY_VALUE, myAdapter.getItem(position));
        startActivity(intent);
    }

    public List<Currency> generateList() {
        final List<Currency> list = new ArrayList<>();
//        Retrofit.getCountries(new Callback<List<Currency>>() {
//            @Override
//            public void success(List<Currency> currencies, Response response) {
//                Toast.makeText(MainActivity.this, "success", Toast.LENGTH_SHORT).show();
//                list.add(currencies.get(0));
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                Toast.makeText(MainActivity.this, "failure", Toast.LENGTH_SHORT).show();
//            }
//        });

        list.add(new Currency(R.mipmap.ic_launcher, "USD", "22.22.22", "usd"));
        list.add(new Currency(R.mipmap.ic_launcher, "GBP", "22.22.22", "gbp"));
        list.add(new Currency(R.mipmap.ic_launcher, "UAH", "22.22.22", "uah"));
        return list;
    }


}
