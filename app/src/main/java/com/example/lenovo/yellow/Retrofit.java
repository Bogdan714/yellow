package com.example.lenovo.yellow;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

public class Retrofit {
    private static final String ENDPOINT = "https://data.fixer.io/api";
    private static ApiInterface apiInterface;

    static {
        initialize();
    }

    interface ApiInterface {
        //@GET("/v2/name/{name}")
        @GET("/latest")
        void getCountries(@Query("access_key") String access_key, Callback<List<Currency>> callback);
    }

    private static void initialize() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getCountries(Callback<List<Currency>> callback) {
        apiInterface.getCountries("aac86b15d493cbbc21d98e6b532bfa85", callback);
    }
}
