package com.example.lenovo.yellow;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListAdapter extends BaseAdapter {

    private Context context;
    private List<Currency> currencies;

    public ListAdapter(Context context, List<Currency> currencies) {
        this.context = context;
        this.currencies = currencies;
    }

    @Override
    public int getCount() {
        return currencies.size();
    }

    @Override
    public Currency getItem(int position) {
        return currencies.get(position);
    }

    @Override
    public long getItemId(int position) {
        return currencies.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        ViewHolder holder;
        if (rowView == null) {
            rowView = LayoutInflater.from(context)
                    .inflate(R.layout.info_currency, parent, false);
            holder = new ViewHolder(rowView);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        holder.icon.setImageResource(getItem(position).getIcon());
        holder.currencyName.setText(getItem(position).getBase());
        holder.currencyInfo.setText(getItem(position).getShortInfo());
        return rowView;
    }

    static class ViewHolder {
        @BindView(R.id.icon)
        ImageView icon;
        @BindView(R.id.name_currency)
        TextView currencyName;
        @BindView(R.id.info_currency)
        TextView currencyInfo;

        public ViewHolder(View root) {
            ButterKnife.bind(this, root);
        }
    }
}
